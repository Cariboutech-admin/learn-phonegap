var slides= {
    "testone": "tested",
    "cards":[
    {
        "catName": "categoryA",
        "settings": [
            {
                "bgColor": "white"
            },
            {
                "fontColor": "orage"
            }
        ],
        "items": [
            {
                "title": "A1",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A1",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A2",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A2",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A3",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A3",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A4",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A4",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A5",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A5",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A6",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A6",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "A7",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big A7",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            }
        ]
    },
    {
        "catName": "categoryB",
        "settings": [
            {
                "bgColor" : "black"
            },
            {
                "fontColor": "blue"
            }
        ],
        "items": [
            {
                "title": "B1",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big B1",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "B2",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big B2",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "B3",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big B3",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "B4",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big B4",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "B5",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big B5",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            }
        ]
    },
    {
        "catName": "categoryC",
        "settings": [
            {
                "bgColor" : "yellow"
            },
            {
                "fontColor": "green"
            }
        ],
        "items": [
            {
                "title": "C1",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C1",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "C2",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C2",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "C3",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C3",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "C4",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C4",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "C5",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C5",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            },
            {
                "title": "C6",
                "section":["section1.html", "section2.html", "section1.html","section2.html","section1.html"],
                "description": ["Bivariate Data, Scatter Plots and Basic Linear Regression", "Comparing Two Data Sets", "Sampling and Early Inference", "Describing the distribution as a Set of Data"],
                "longtitle": "big C6",
                "longDescription": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat.",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velitrhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                ]
            }
        ]
    }
]
}
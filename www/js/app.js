'use strict';

// create the module and name it scotchApp

var caribouApp = angular.module('caribouApp', ['ngRoute', 'caribouControllers']);

caribouApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'partials/home.html',
            controller: 'HomeCtrl'
        })

        .when('/team', {
            templateUrl: 'partials/team.html',
            controller: 'TeamCtrl'
        })

        .when('/team/:id', {
            templateUrl: 'partials/employee.html',
            controller: 'EmployeeCtrl'
        })

        .when('/contact', {
            templateUrl: 'partials/contact.html',
            controller: 'ContactsCtrl'
        })

        .when('/learn', {
            templateUrl: 'partials/learn.html',
            controller: 'LearnCtrl'
        })
})


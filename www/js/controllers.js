'use strict';

var caribouControllers = angular.module('caribouControllers', ['ui.bootstrap']);

// create the controller and inject Angular's $scope
caribouControllers.controller('HomeCtrl', ['$scope', function($scope) {
    $scope.message = 'Everyone come and see how good I look!';
}]);

caribouControllers.controller('TeamCtrl', ['$scope', '$http',
    function($scope, $http) {
        $scope.message = 'We have got an awesome team!';
        $http.get('employees/employees.json').success(function(data) {
            $scope.employees = data;
        });
    }]);

caribouControllers.controller('ContactsCtrl', function($scope) {
    $scope.message = 'Contact Us!';
});

caribouControllers.controller('EmployeeCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {
        $http.get('employees/employees.json').success(function(data) {
            for(var i = 0; i < data.length; i += 1){
                var result = data[i];
                if(result.id == $routeParams.id){
                    $scope.employee = result;
                }
            }
        });
    }]);

caribouControllers.service('sharedService', function($rootScope) {
    var loadLpDetailsService = {};

    loadLpDetailsService.selectedItem = '';

    loadLpDetailsService.prepForBroadcast = function(item) {
        this.selectedItem = item;
        this.broadcastItem();
    };

    loadLpDetailsService.broadcastItem = function() {
        $rootScope.$broadcast('lpNameChanged');
    };

    return loadLpDetailsService;
});

caribouControllers.controller('LearnCtrl', ['$scope', '$http', 'sharedService', function($scope, $http, loadLpDetailsService){
    $scope.loadTabs = function() {
        console.log('loading tabs...');
        
        var responsePromise = $http.get("https://script.google.com/macros/s/AKfycbyTd5P4W7B0g8nPkMul-6WGImj4WBB72ADtKosgTnqEv5_HiGnd/exec");
        
        responsePromise.success(function(data, status, headers, config) {
        	 var tabs= data;
             $scope.tabs = tabs;
             console.log('data from server : ' + tabs);
        });
        responsePromise.error(function(data, status, headers, config) {
        	console.log('error');
        });
    }
    $scope.loadTabs();

    $scope.onTabSelected = function(selectedItem) {
        console.log(selectedItem);
        loadLpDetailsService.prepForBroadcast(selectedItem);
    };
}]);

caribouControllers.controller('DetailPanelCtrl', function($scope){
	$scope.myFunction = function (e) {
	    //close the panel
		$('.contract').prop('class','display');
    	$('#detailpanelid').hide();
	  };
});

caribouControllers.controller('CarouselCardCtrl', ['$scope', '$http', 'sharedService', function($scope, $http, loadLpDetailsService){
    $scope.myInterval = -1;
    
    $scope.displayPanel = function(event)
    {
    	
    	$('.display').prop('class','contract');
    	$('#detailpanelid').show();
    	
       // var target = angular.element(event.target).parent();
        //target.addClass('contract');
       // console.log('target: '+target.html());
        
        //this target is a jQuery lite object
        // now you can do what you want through the target
    };

    $scope.$on('lpNameChanged', function() {
        console.log('loading lp details...');
        $http({
            url: 'https://script.google.com/macros/s/AKfycbyTd5P4W7B0g8nPkMul-6WGImj4WBB72ADtKosgTnqEv5_HiGnd/exec?worksheetno=' + loadLpDetailsService.selectedItem,            
            method: 'GET',
            // pass the parameters
            params: {worksheetno: loadLpDetailsService.selectedItem}
            //params: {arg: "lp-details", sheetName: "Counting"}
        })
            .success(function(data) {
                $scope.slides = data;
                console.log('data : ' + JSON.stringify(data));
            })
            .error(function(response) {
                console.log('error loading lp details');
            });
    });
}]);
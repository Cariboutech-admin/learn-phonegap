var slides = [
    {
        "testone": "tested",
        "cards":[
            {
                "catName": "categoryA",
                "settings": [
                    {
                        "bgColor": "white"
                    },
                    {
                        "fontColor": "orage"
                    }
                ],
                "items": [
                    {
                        "title": "A1",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A2",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A3",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A4",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A5",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A3",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A4",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "A5",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    }

                ]
            },
            {
                "catName": "categoryB",
                "settings": [
                    {
                        "bgColor" : "black"
                    },
                    {
                        "fontColor": "blue"
                    }
                ],
                "items": [
                    {
                        "title": "B1",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "B2",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "B3",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    }
                ]
            },
            {
                "catName": "categoryC",
                "settings": [
                    {
                        "bgColor" : "yellow"
                    },
                    {
                        "fontColor": "green"
                    }
                ],
                "items": [
                    {
                        "title": "C1",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    },
                    {
                        "title": "C2",
                        "description": "sample",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies felis sed justo volutpat, eget iaculis velit rhoncus. Proin pharetra velit sed augue posuere hendrerit. Aliquam erat volutpat."
                    }
                ]
            }
        ]
    }
];
'use strict';

/* Services */

var caribouServices = angular.module('caribouServices', ['ngResource']);

caribouServices.factory('Employee', ['$resource',
  function($resource){
    return $resource('phones/:phoneId.json', {}, {
      query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
    });
  }]);
